//
//  CodeScanView.h
//  简单app项目
//
//  Created by Guo on 2020/05/22.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^BackQRCodeURL)(NSString  *stringValue);

@interface CodeScanView : UIView

/**扫描回调*/
@property (nonatomic,copy) BackQRCodeURL backQRCodeURL;
@end

NS_ASSUME_NONNULL_END
