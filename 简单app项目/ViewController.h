//
//  ViewController.h
//  简单app项目
//
//  Created by Guo on 2020/03/28.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface ViewController : UIViewController <UNUserNotificationCenterDelegate>
//用户通知协议 用于接受通知以及通知相关的所有操作


@end

