//
//  WebViewController.h
//  简单app项目
//
//  Created by Guo on 2020/03/28.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebKit/WebKit.h"

//获取屏幕宽度、高度
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
// 状态栏高度
#define STATUSBAR_HEIGHT [UIApplication sharedApplication].windows.firstObject.windowScene.statusBarManager.statusBarFrame.size.height
// 导航栏高度
#define NAVIBAR_HEIGHT self.navigationController.navigationBar.frame.size.height
//获取导航栏+状态栏的高度
#define NAVANDSTATUS_HEIGHT  (NAVIBAR_HEIGHT + STATUSBAR_HEIGHT)
// 工具栏高度
#define TOOLBAR_HEIGHT  self.navigationController.toolbar.frame.size.height
#define FTPURL        @"smb://guodembp"

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController
<
WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler,UINavigationControllerDelegate, UIImagePickerControllerDelegate,NSURLSessionDownloadDelegate,UIGestureRecognizerDelegate
>
{
    UILabel *progressLabel;
}
@property (nonatomic, strong) WKWebView *currentWebView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *foregroundView;
//@property (nonatomic, strong) UILabel *progressLabel;

@end

NS_ASSUME_NONNULL_END
