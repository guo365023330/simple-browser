//
//  UrlCollectionView.m
//  简单app项目
//
//  Created by Guo on 2020/05/15.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "UrlCollectionView.h"

@interface UrlCollectionView()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIImageView *faviconView;
@property (nonatomic, strong) UICollectionViewCell *snapedViewCell;
@property (nonatomic, strong) UIView *snapedView;
@property (nonatomic, strong) NSMutableArray *urlArray;
@property (nonatomic, strong) NSMutableArray *cellArray;
@property (nonatomic, assign) CGPoint beginPoint;
@property (nonatomic, assign) CGRect frameRect;

@end

@implementation UrlCollectionView
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) createContentViewWithFrame:(CGRect)frame UrlArray:(NSMutableArray*)urlArray
{
    _frameRect = frame;
    self.frame = frame;
    _urlArray = urlArray;
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.collectionView];
}


-(UICollectionView*) collectionView {
    if(!_collectionView){
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        float itemWith = ([UIScreen mainScreen].bounds.size.width - 30 - 30 )/4;
        flowLayout.itemSize = CGSizeMake(itemWith, itemWith + 15);
        flowLayout.minimumLineSpacing = 10;//左右间距
        flowLayout.minimumInteritemSpacing = 10;
        flowLayout.sectionInset = UIEdgeInsetsMake(5, 5, 10, 10);//边缘最小距离
       _collectionView = [[UICollectionView alloc] initWithFrame:_frameRect collectionViewLayout:flowLayout];
       [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ViewIdentifer"];
       _collectionView.backgroundColor = [UIColor whiteColor];
       _collectionView.delegate = self;
       _collectionView.dataSource = self;
       _collectionView.scrollEnabled = YES;
    }
        //[self addSubview:_collectionView];
        return _collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _urlArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell  = [collectionView dequeueReusableCellWithReuseIdentifier:@"ViewIdentifer" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:arc4random()%255/255.0 green:arc4random()%255/255.0 blue:arc4random()%255/255.0 alpha:1];
    NSString *faviconUrlString = [NSString stringWithFormat:@"%@%s", _urlArray[indexPath.item], "/favicon.ico"];
    NSURL *faviconUrl = [NSURL URLWithString:faviconUrlString];
    NSData *favicondata = [NSData dataWithContentsOfURL:faviconUrl];
    CGRect imageViewRect = CGRectMake(15, 20, 35, 35);
    _faviconView = [[UIImageView alloc]initWithFrame:imageViewRect];
    UIImage *favicon = [UIImage imageWithData:favicondata];
    [favicon drawInRect: imageViewRect];
    _faviconView.image = favicon;
    [cell addSubview:_faviconView];
    cell.contentMode = UIViewContentModeCenter;//居中现显示
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGesture:)];
    longPress.minimumPressDuration = 2;//最低按压时间
    [cell addGestureRecognizer:longPress];
    [_cellArray addObject:cell];
    return cell;
}

- (IBAction)longPressGesture:(UILongPressGestureRecognizer *)sender {
    CGPoint point = [sender locationInView:self.collectionView];
    CGPoint point1 = CGPointMake(point.x + 5, point.y + 5);
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
    switch (sender.state) {
        case UIGestureRecognizerStateBegan: {
            ///识别成功是这个case.可以在这里写识别成功的代码
            _snapedViewCell = [_collectionView cellForItemAtIndexPath:indexPath];
            _snapedViewCell.hidden = YES;
            self.snapedView = [_snapedViewCell snapshotViewAfterScreenUpdates:NO];//yes的话得到空白
            self.snapedView.hidden = NO;
            self.snapedView.alpha = 0.5;
            self.snapedView.frame = CGRectMake(_snapedViewCell.frame.origin.x, _snapedViewCell.frame.origin.y, _snapedViewCell.frame.size.width*1.5, _snapedViewCell.frame.size.height*1.5);
            [self addSubview:_snapedView];
            self.snapedView.center = point1;
            _beginPoint = point1;
            [_collectionView beginInteractiveMovementForItemAtIndexPath:indexPath];
        }
            break;
        case UIGestureRecognizerStateChanged: {
            self.snapedView.center = point1;
            [_collectionView updateInteractiveMovementTargetPosition:point];
        }
            break;
        case UIGestureRecognizerStateEnded: {
            ///手势结束(比如手离开了屏幕)
            _snapedView.hidden = YES;
            [_snapedView removeFromSuperview];
            _snapedView = nil;
            _snapedViewCell.hidden = NO;
            [_collectionView endInteractiveMovement];
        }
        default:
            _snapedViewCell.hidden = NO;
            _snapedView.hidden = YES;
            [_collectionView cancelInteractiveMovement];
            break;
    }
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath*)destinationIndexPath {
    // 替换拖拽数据
    [_cellArray exchangeObjectAtIndex:destinationIndexPath.row withObjectAtIndex:sourceIndexPath.row];
    [_urlArray exchangeObjectAtIndex:destinationIndexPath.row withObjectAtIndex:sourceIndexPath.row];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.urlCollectionCellBlock){
        NSURL *collectionUrl = [NSURL URLWithString:_urlArray[indexPath.item]];
        self.urlCollectionCellBlock(collectionUrl);
    }
}

@end
