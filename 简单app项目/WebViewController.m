//
//  WebViewController.m
//  简单app项目
//
//  Created by Guo on 2020/03/28.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "WebViewController.h"
#import "DropDownMenuView.h"
#import "UrlCollectionView.h"
#import "CodeScanView.h"
#import "UserInfoController.h"
#import "DeveloperTaskView.h"
#import <AVFile.h>

//@protocol WKScriptMessageHandler <NSObject>

@interface WebViewController ()

@property (nonatomic, strong) UITextField *textFiled;
@property (nonatomic, strong) UILabel *pageCountLable;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) WKNavigationAction *navigationAction;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) NSArray *arrayBtn;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, assign) NSInteger pageCount;
@property (nonatomic, assign) NSInteger menuTag;
@property (nonatomic, strong) NSMutableArray *WebViewArray;
@property (nonatomic, strong) NSMutableArray *urlArray;
@property (nonatomic, assign) CGRect webPageRect;
@property (nonatomic, strong) UIBarButtonItem *backwardBtn;
@property (nonatomic, strong) UIBarButtonItem *forewardBtn;
@property (nonatomic) BOOL isKeyBoardShow;
@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _WebViewArray = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor grayColor];
    self.navigationController.navigationBar.barTintColor = [UIColor yellowColor];
    self.navigationController.toolbar.barTintColor=[UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    //self.title = @"浏览界面";
    [self initUrlArray];
    [self makeBtn];
    [self makeWKWebView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // 这里要记得移除handlers
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"noParamsFunction"];
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"haveParamsFunction"];
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"imageRead"];
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"takePhoto"];
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"uploadPhoto"];
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"loadPhoto"];
    [_currentWebView.configuration.userContentController removeScriptMessageHandlerForName:@"loadToLocal"];
}

// 进度条
- (void) makePgView{
    CGRect rect = CGRectMake(0, 0, SCREEN_WIDTH, 3);
    _progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    _progressView.frame = rect;
    [_progressView setTrackTintColor:[UIColor whiteColor]];
    _progressView.progressTintColor = [UIColor magentaColor];
    [_currentWebView addSubview:_progressView];
    [_currentWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [_currentWebView addObserver:self forKeyPath:@"URL" options:NSKeyValueObservingOptionNew context:@"Url"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardHide) name:UIKeyboardDidHideNotification object:nil];
};

//创建wkwebview
- (void) makeWKWebView {
    WKWebView *webview = [[WKWebView alloc] init];
    //设置网页的配置文件
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    //允许视频播放
    configuration.allowsAirPlayForMediaPlayback = YES;
    // 允许在线播放
    configuration.allowsInlineMediaPlayback = YES;
    // 允许可以与网页交互，选择视图
    configuration.selectionGranularity = YES;
    //默认字体大小
    configuration.preferences.minimumFontSize = 9.0;
    //添加监控事件
    //configuration.userContentController = [WKUserContentController new];
    WKUserContentController * userContent = configuration.userContentController;

    //注册js要调用的方法名
    [userContent addScriptMessageHandler:self name:@"noParamsFunction"];
    [userContent addScriptMessageHandler:self name:@"haveParamsFunction"];
    [userContent addScriptMessageHandler:self name:@"imageRead"];
    [userContent addScriptMessageHandler:self name:@"takePhoto"];
    [userContent addScriptMessageHandler:self name:@"uploadPhoto"];
    [userContent addScriptMessageHandler:self name:@"loadPhoto"];
    [userContent addScriptMessageHandler:self name:@"loadToLocal"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"URLCHANGENOTIFY" object:_currentWebView.URL userInfo:nil];
    [[NSNotificationCenter defaultCenter]addObserver:_currentWebView.URL selector:@selector(setCurrentUrl) name:@"URLCHANGENOTIFY" object:nil];
    
    //webview创建
    _webPageRect = CGRectMake(0, 3, SCREEN_WIDTH, SCREEN_HEIGHT - NAVANDSTATUS_HEIGHT - TOOLBAR_HEIGHT );
    webview = [[WKWebView alloc] initWithFrame:_webPageRect configuration:configuration];
    webview.backgroundColor = [UIColor greenColor];
    // UI代理
    webview.UIDelegate = self;
    // 导航代理
    webview.navigationDelegate = self;
    // 是否允许手势左滑返回上一级, 类似导航控制的左滑返回
    webview.allowsBackForwardNavigationGestures = YES;
    // 这行代码可以是侧滑返回webView的上一级，而不是根控制器（*只针对侧滑有效）
    [webview setAllowsBackForwardNavigationGestures:true];
    //可返回的页面列表, 存储已打开过的网页
    //WKBackForwardList * backForwardList = [webview backForwardList];
    [self.view addSubview:webview];
    [_WebViewArray addObject:webview];
    _currentWebView = webview;
    [self makePgView];
    [self loadBookMark];
}

- (void) initUrlArray {
    NSString *pathString = [[NSBundle mainBundle]pathForResource:@"userData" ofType:@"plist"];
    NSDictionary *dic = [[NSDictionary alloc]initWithContentsOfFile:pathString];
    NSArray *array = [dic objectForKey:@"BASE_URL"];
    _urlArray = [NSMutableArray arrayWithArray:array];
}

- (void) jsWebView {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ab.html" ofType:nil];
    [_currentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
}

- (void) keyBoardHide {
    _isKeyBoardShow = YES;
}

- (NSString*) getActiveELement
{
    __block NSString *result = @"";
    [_currentWebView evaluateJavaScript:@"window.external.checkTargetElement()" completionHandler:^(id _Nullable response, NSError * _Nullable error) {
        if (error == nil){
            if (response != nil) {
                result = [NSString stringWithFormat:@"%@", response];
            }
        }else{
            NSLog(@"error is:  %@", error);
        }
    }];
    return result;
}

- (void)addUserScript{
    if (_currentWebView.configuration.userContentController == nil) {
        return;
    }
    
    NSString *pathString = [[NSBundle mainBundle] pathForResource:@"1.main" ofType:@"js"];
    NSError *error;
    NSString *exFunc = [[NSString alloc]initWithContentsOfFile:pathString encoding:NSUTF8StringEncoding error:&error];
    WKUserScript *exFuncScript = [[WKUserScript alloc] initWithSource:[exFunc copy] injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];//formainframe:一个布尔值，指示脚本是仅应注入主框架（YES）还是注入所有框架（NO）。
    [_currentWebView.configuration.userContentController addUserScript:exFuncScript];
}


-(void)makeBtn{
    self.navigationController.toolbarHidden = NO;
    //底部工具栏-退回
    _backwardBtn = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"backward.png"] imageWithRenderingMode:UIImageRenderingModeAutomatic] style:UIBarButtonItemStylePlain target:self action:@selector(barBtnClick:)];
    _backwardBtn.tag = 100;
    [_backwardBtn setBackgroundImage:[UIImage imageNamed:@"backward-gray.png"] forState:UIControlStateDisabled barMetrics:0];
    _backwardBtn.enabled = NO;
    
    //底部工具栏-主页
    UIBarButtonItem *btnMain = [[UIBarButtonItem alloc] initWithTitle:@"本地网页" style:UIBarButtonItemStylePlain target:self action:@selector(barBtnClick:)];
    btnMain.tag = 101;
    
    //底部工具栏-前进
    _forewardBtn = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"foreward.png"] imageWithRenderingMode:UIImageRenderingModeAutomatic] style:UIBarButtonItemStylePlain target:self action:@selector(barBtnClick:)];
    _forewardBtn.tag = 102;
    [_forewardBtn setBackgroundImage:[UIImage imageNamed:@"foreward-gray.png"] forState:UIControlStateDisabled barMetrics:1];
    _forewardBtn.enabled = NO;
     
    //固定宽度占位按钮
    UIBarButtonItem *btnF01 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]; //自动计算间隙
    
    // JS交互的按钮
    UIBarButtonItem *jsBtn1 = [[UIBarButtonItem alloc] initWithTitle:@"2js无参" style:UIBarButtonItemStylePlain target:self action:@selector(barBtnClick:)];
    jsBtn1.tag = 300;
    
    UIBarButtonItem *jsBtn2 = [[UIBarButtonItem alloc] initWithTitle:@"2js有参" style:UIBarButtonItemStylePlain target:self action:@selector(barBtnClick:)];
    jsBtn2.tag = 301;
    
    _arrayBtn = [NSArray arrayWithObjects:_backwardBtn,btnF01,jsBtn1,btnF01,btnMain,btnF01,jsBtn2, btnF01, _forewardBtn,nil];
    self.toolbarItems = _arrayBtn;
    
    //导航栏按钮
    UIBarButtonItem *backToMain = [[UIBarButtonItem alloc] initWithTitle:@"主页" style:UIBarButtonItemStylePlain target:self action:@selector(barBtnClick:)];
    self.navigationItem.leftBarButtonItem = backToMain;
    backToMain.tag = 200;
    
    //更多选项
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(barBtnClick:)];
    self.navigationItem.rightBarButtonItem = rightBtn;
    rightBtn.tag = 201;
    
    //网页数量countLable
    CGRect rectView = CGRectMake(0.8*SCREEN_WIDTH, NAVIBAR_HEIGHT/2 - 14, 25, NAVIBAR_HEIGHT/2);
    _pageCountLable = [[UILabel alloc]initWithFrame:rectView];
    _pageCountLable.backgroundColor = [UIColor yellowColor];
    _pageCountLable.textAlignment = NSTextAlignmentCenter;
    _pageCountLable.textColor = [UIColor grayColor];
    _pageCountLable.layer.borderWidth = 2;
    _pageCountLable.layer.borderColor = [[UIColor grayColor] CGColor];
    [self.navigationController.navigationBar addSubview:self.pageCountLable];
    
    //网址输入栏
    _textFiled=[[UITextField alloc] initWithFrame:CGRectMake(65, NAVIBAR_HEIGHT/2 - 14, SCREEN_WIDTH - 150, NAVIBAR_HEIGHT/2 + 6)];
    [_textFiled setBorderStyle:UITextBorderStyleRoundedRect];
    _textFiled.layer.cornerRadius = 20;
    [self.navigationController.navigationBar addSubview:_textFiled];
}

-(void)makedownloadView{
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(20, 106, 300, 44)];
    _backgroundView.backgroundColor = [UIColor lightGrayColor];
    _backgroundView.tag = 400;
    
    _foregroundView = [[UIView alloc] initWithFrame:CGRectMake(25, 110, 0, 36)];
    _foregroundView.backgroundColor = [UIColor greenColor];
    _foregroundView.tag = 401;
    
    progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(22, 160, 280, 36)];
    progressLabel.textAlignment = NSTextAlignmentCenter;
    progressLabel.tag = 402;
    [self.view addSubview:_backgroundView];
    [self.view addSubview:_foregroundView];
    [self.view addSubview:progressLabel];//实例对象
    NSURL *imageurl = [NSURL URLWithString:@"http://lc-1J8RGXUM.cn-n1.lcfile.com/TGgYUIKdbg0BuCdXSsOKBiC"];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:imageurl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    NSURLSessionConfiguration *congig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:congig delegate:self delegateQueue:nil];
    NSURLSessionDownloadTask *task = [urlSession downloadTaskWithRequest:request];
    [task resume];
}


- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSString *originalPath = [location path];//url To string 临时存储路径
    NSLog(@"original location: %@", originalPath);
    //图片目标的存储位置
    NSString *targetPath = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/logo.png"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:targetPath])
    {
        //下载的图片从临时位置移动到目标位置，如果移动时出现错误，
        //将错误信息存储在错误对象里
        NSError *error;
        [fileManager moveItemAtPath:originalPath toPath:targetPath error:&error];
        if (error == nil){
            NSLog(@"new location: %@", targetPath);
        }else{
            NSLog(@"error: %@", [error description]);
        }
    }
}

//响应下载进度的代理方法 bytesWritten： 每次写入的data字节数
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    float rate = totalBytesWritten/totalBytesExpectedToWrite;//下载数据量比值
    //从一个分离线程，切换至主线程，以进行页面的改动
    dispatch_sync(dispatch_get_main_queue(),^{
        self.foregroundView.frame = CGRectMake(self.foregroundView.frame.origin.x, self.foregroundView.frame.origin.y, rate * 280, self.foregroundView.frame.size.height);
        if (rate == 1.0){
            progressLabel.text = @"Finished 😄";
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
        tap.delegate = self;
        [_currentWebView.scrollView addGestureRecognizer:tap]; //wkwebview 添加touch在scrollview上
    });
}

//点击wkwebview 事件代理
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

//关闭进度提示
- (void) singleTap
{
    progressLabel.text = nil;
    for(UIView *subview in [self.view subviews])
    {
        if (subview.tag == 400 || subview.tag == 401 || subview.tag == 402) {
            [subview performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:NO];
        }
    }
}

//响应任务恢复的事件
- (void) URLSession:(NSURLSession *)session downloadTask:(nonnull NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    NSLog(@"响应任务恢复%lld", fileOffset);
}

- (void) loadWebView:(NSURL *)url
{
    if (url!= nil){
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_currentWebView loadRequest:request];
    }else{
        url = [NSURL URLWithString:@"http://www.google.com/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [_currentWebView loadRequest:request];
    }
    [self addUserScript];
}
/**
 *  在发送请求之前，决定是否跳转
 *  @param webView          实现该代理的webview
 *  @param navigationAction 当前navigation
 *  @param decisionHandler  是否调转block
 */
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString * urlStr = navigationAction.request.URL.absoluteString;
   /*
    - (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if (navigationAction.navigationType==WKNavigationTypeBackForward) {                  //判断是返回类型
        if (_currentWebView.backForwardList.backList.count>0) {                                  //得到栈里面的list
            WKBackForwardListItem * item = webView.backForwardList.currentItem;          //得到现在加载的list
            for (WKBackForwardListItem * backItem in webView.backForwardList.backList) { //循环遍历，得到你想退出到
                //添加判断条件
                [_currentWebView goToBackForwardListItem:[_currentWebView backForwardList.backListfirstObject]];
            }
        }
    }
    */
    //自己定义的协议头
    NSString *htmlHeadString = @"github://";
    if([urlStr hasPrefix:htmlHeadString]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"通过截取URL调用OC" message:@"你想前往我的Github主页?" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }])];
        [alertController addAction:([UIAlertAction actionWithTitle:@"打开" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:[urlStr stringByReplacingOccurrencesOfString:@"github://callName_?" withString:@""]];
          //  [[UIApplication sharedApplication] openURL:url];
        }])];
        [self presentViewController:alertController animated:YES completion:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
    }else{
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation
{
    _progressView.hidden = NO;
    //防止progressView被网页挡住
    [_currentWebView bringSubviewToFront:self.progressView];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error;
{
    _progressView.hidden = YES;
    NSLog(@"加载失败:%@",error);
}


- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView API_AVAILABLE(macosx(10.11), ios(9.0));
{
    [webView reload];
}

- (WKWebView*)webView:(WKWebView*)webView createWebViewWithConfiguration:(WKWebViewConfiguration*)configuration forNavigationAction:(WKNavigationAction*)navigationAction windowFeatures:(WKWindowFeatures*)windowFeatures {
    NSLog(@"页面弹出窗口");
    if(!navigationAction.targetFrame.isMainFrame) {
        [_currentWebView loadRequest:navigationAction.request];
    }
    return nil;
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    
}

- (void) barBtnClick:(UIBarButtonItem*)barBtn{
    switch (barBtn.tag) {
        case 100:
            if (_currentWebView.canGoBack) {
               // WKBackForwardListItem.
                [_currentWebView goBack];
                _backwardBtn.enabled = _currentWebView.canGoBack;
                _forewardBtn.enabled = _currentWebView.canGoForward;
                // WKBackForwardListItem.
            }
            break;
        case 101:
            {
                //加载本地服务器html文件
                #if TARGET_IPHONE_SIMULATOR
                    NSString * filePath =  [[NSBundle mainBundle] pathForResource:@"ab" ofType:@"html"];
                    NSURL * baseUrl = [[NSBundle mainBundle] bundleURL];
                    [_currentWebView loadHTMLString:[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil] baseURL:baseUrl];
                //加载内嵌html文件
                #else
                    NSURL * baseUrl =  [NSURL URLWithString:@"http://127.0.0.1/ab.html"];
                    NSURLRequest *baseRequest = [NSURLRequest requestWithURL:baseUrl];
                    [_currentWebView loadRequest:baseRequest];
                   // [_currentWebView loadHTMLString:[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil] baseURL:baseUrl];}
                #endif
                for (UIView *view in self.currentWebView.subviews) {
                    if([view isKindOfClass:[UrlCollectionView class]]){
                        [view removeFromSuperview];
                    }
                }
            }
            break;
        case 102:
            if (_currentWebView.canGoForward) {
                [_currentWebView goForward];
                _backwardBtn.enabled = _currentWebView.canGoBack;
                _forewardBtn.enabled = _currentWebView.canGoForward;
            }
            break;
        case 200:
            //返回登陆界面
            [self backToMainPage];
            break;
        case 201:
            /** 显示下拉菜单 */
            //创建实例
            //示例调用
            @autoreleasepool {
                DropDownMenuView *menuView = [[DropDownMenuView alloc] init];
                [menuView setupDropDownMenu];
                //关闭键盘
                _isKeyBoardShow = NO;
                [_currentWebView endEditing:YES];
//                while (_isKeyBoardShow == NO) {
//                    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]]; //卡线程
//                }
                NSString *elementName = [self getActiveELement];
                if (elementName != nil) {
                    [_currentWebView evaluateJavaScript:@"document.activeElement.blur()" completionHandler:nil];
                }
                //block传值-实现block接收传值
                self.menuTag = -1;
                menuView.passRowNumBlock = ^(NSInteger menuTag){
                    self.menuTag = menuTag;
                    switch (self.menuTag) {
                        case 0:
                            //新开网页
                            [self makeWKWebView];
                            self.pageCount += 1;
                            self.pageCountLable.text = [NSString stringWithFormat:@"%ld", self.pageCount];
                            break;
                        case 1:
                            //刷新网页
                            self.url = [NSURL URLWithString:self.textFiled.text];
                            [self loadWebView:self.url];
                            break;
                        case 2:
                            {
                                //关闭当前网页
                                self.currentWebView .navigationDelegate = nil;
                                self.currentWebView .UIDelegate = nil;
                                [self.currentWebView stopLoading];
                                [self.currentWebView removeFromSuperview];
                                [self.WebViewArray removeObject:self.currentWebView];
                                self.pageCount -=1;
                                self.pageCountLable.text = [NSString stringWithFormat:@"%ld", self.pageCount];
                                self.currentWebView = [self.WebViewArray objectAtIndex:self.pageCount];
                                self.url = [NSURL URLWithString:self.textFiled.text];
                                //self.currentWebView = self.WebViewArray
                                [self.currentWebView reload];
                                break;
                            }
                        case 3:
                        //QRCode读取
                            {
                                CodeScanView *scanView = [[CodeScanView alloc] initWithFrame:self.webPageRect];
                                [self.currentWebView addSubview:scanView];
                                scanView.backQRCodeURL = ^(NSString *stringValue){
                                    self.url = [NSURL URLWithString:stringValue];
                                    [self loadWebView:(self.url)];
                                    for (UIView *view in self.currentWebView.subviews) {
                                        if([view isKindOfClass:[CodeScanView class]]){
                                            [view removeFromSuperview];
                                        }
                                    }
                                };
                            }
                        break;
                        case 4:
                        //主页标签
                            [self loadBookMark];
                            break;
                        case 5:
                        //購買登録
                            {
                                UserInfoController *userView = [[UserInfoController alloc]initWithNibName:@"UserInfoController" bundle:[NSBundle mainBundle]];
                                [self.navigationController pushViewController:userView animated:YES];
                            }
                        break;
                        case 6:
                        //多线程-系统状态显示
                            {//有变量加括号
                                DeveloperTaskView *taskView = [self.view viewWithTag:DEVELOPER_TAKVIEW_TAG];
                                if(taskView == nil){
                                    CGRect taskFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width/2 - TASK_VIEW_W/2), ([UIScreen mainScreen].bounds.size.height/2 - TASK_VIEW_H/2), TASK_VIEW_W, TASK_VIEW_H);
                                    taskView = [[DeveloperTaskView alloc]initWithFrame:taskFrame];
                                    [self.view addSubview:taskView];
                                    [taskView startThread:1.0];
                                }else{
                                    [taskView endThread];
                                    [taskView removeFromSuperview];
                                    taskView = nil;
                                }
                            }
                        break;
                        default:
                            break;
                    }};
            }
        case 300:
            //在这里返回给h5信息
            [_currentWebView evaluateJavaScript:@"transferJSAction()" completionHandler:nil];
            break;
        case 301:
            //在这里返回给h5信息
            //信息的格式可以是任何形式的，这里用字符串
            {NSString * str = @"OC传递给h5的内容";
            NSString * sendMessage = [NSString stringWithFormat:@"transferJSActionAndMessage('%@')",str];
            [_currentWebView evaluateJavaScript:sendMessage completionHandler:^(id _Nullable response, NSError * _Nullable error) {
                NSLog(@"error %@", error);//可以打印错误信息
            }];
            }
            break;
        default:
            break;
    }
}

- (void)backToMainPage{
    for (UIView *view in self.navigationController.navigationBar.subviews ) {
        [view removeFromSuperview];
    }//回到主页前移除subview
    [self.navigationController setToolbarHidden:YES animated:nil];//回到主页时底部隐藏toobar
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
}

- (void)loadBookMark{
    UrlCollectionView *contentView = [[UrlCollectionView alloc] init];
    [contentView createContentViewWithFrame:self.webPageRect UrlArray:self.urlArray];
    [self.currentWebView addSubview:contentView];
    self.textFiled.text = @"local";
    contentView.urlCollectionCellBlock = ^(NSURL *url) {
        [self loadWebView:(url)];
        for (UIView *view in self.currentWebView.subviews) {
            if([view isKindOfClass:[UrlCollectionView class]]){
                [view removeFromSuperview];
            }
        }
    };
}

#pragma mark - 在该方法里面进行与js的交互
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    if ([message.name isEqualToString:@"noParamsFunction"]) {
        NSLog(@"收到js端端信息，但是没有带任何参数， %@", message.body);
    }
    if ([message.name isEqualToString:@"haveParamsFunction"]) {
        NSLog(@"收到js端发送端信息。%@",message.body);
    }
    if ([message.name isEqualToString:@"imageRead"]) {
        NSLog(@"收到js端发送端信息。%@",message.body);
        //从相册选取
        UIImagePickerController *Picker = [[UIImagePickerController alloc] init];
        Picker.delegate = (id)self;
        [self.navigationController presentViewController:Picker animated:YES completion:nil];
    }
    if ([message.name isEqualToString:@"takePhoto"]) {
        NSLog(@"收到js端发送端信息。%@",message.body);
           //拍照
        [self takePhoto];
    }
    if ([message.name isEqualToString:@"uploadPhoto"]) {
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        if (_imageData != nil){
            AVFile *file = [AVFile fileWithData:_imageData];
            [file uploadWithCompletionHandler:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"アップロード成功" preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction: ok];
                    [self presentViewController:alert animated:YES completion:nil];
                } else {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"アップロード失败" preferredStyle:UIAlertControllerStyleActionSheet];
                    [alert addAction: ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"请选择上传对象" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction: ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    if ([message.name isEqualToString:@"loadToLocal"]) {
        [self makedownloadView];
    }
    if ([message.name isEqualToString:@"loadPhoto"]) {
        //AVFile *file = [AVFile fileWithRemoteURL:@"http://lc-1J8RGXUM.cn-n1.lcfile.com/TGgYUIKdbg0BuCdXSsOKBiC"];
        NSString *urlString = @"http://lc-1J8RGXUM.cn-n1.lcfile.com/TGgYUIKdbg0BuCdXSsOKBiC";
        //NSString *imageString = [self removeSpaceAndNewline:encodedImageStr];
        NSString * sendMessage = [NSString stringWithFormat:@"imageLoadFromUrl('%@')",urlString];
        [_currentWebView evaluateJavaScript:sendMessage completionHandler:^(id _Nullable response, NSError * _Nullable error) {
            NSLog(@"error: %@", error);//可以打印错误信息
        }];
    }
    if ([message.name isEqualToString:@""]) {
        [self makedownloadView];
    }
}

//拍照
- (void)takePhoto{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.delegate = (id)self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self.navigationController presentViewController:picker animated:YES completion:^{
            NSLog(@"OK");
        }];
    }
    else {
        NSLog(@"模拟其中无法打开照相机，请在真机中使用");
    }
}

// 相册
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    _imageData = UIImageJPEGRepresentation(image, 0.001);
    
    // imagePicker.delegate = self;
    NSString *encodedImageStr = [_imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *imageString = [self removeSpaceAndNewline:encodedImageStr];
    NSString *sendMessage = [NSString stringWithFormat:@"imageSet('%@')",imageString];
    
    // 这里传值给h5界面
    [_currentWebView evaluateJavaScript:sendMessage completionHandler:^(id _Nullable response, NSError * _Nullable error) {
        NSLog(@"value图片: %@ error1: %@", response, error);
    }];
}
        
//删除字符串中的空格和换行符等
- (NSString *)removeSpaceAndNewline:(NSString *)str
{
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return temp;
}
         

#pragma mark - kvo监听
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if (context == @"Url" && [keyPath isEqualToString:@"URL"]) {
         NSString *newpageurl = [NSString stringWithString: _currentWebView.URL.absoluteString];
         if (_url.absoluteString != newpageurl) {
             _textFiled.text = [NSString stringWithString: newpageurl];
             _url = _currentWebView.URL;
             _progressView.hidden = YES;
             if (_currentWebView.canGoBack) {
                 _backwardBtn.enabled = YES;
             }else{
                 _backwardBtn.enabled = NO;
             }
             if (_currentWebView.canGoForward) {
                 _forewardBtn.enabled = YES;
             }else{
                 _forewardBtn.enabled = NO;
             }
         }
    }else{
        if ([keyPath isEqualToString:@"estimatedProgress"]) {
            _progressView.hidden = NO;
            _progressView.progress = _currentWebView.estimatedProgress;
        }
        CGFloat progress = [[change objectForKey:NSKeyValueChangeNewKey] doubleValue];
        if (progress == 1) {
            _progressView.progress = 0.0f;
        }else{
            [_progressView setProgress:progress animated:YES];
        }
    }
}

- (void)dealloc{
    [_currentWebView removeObserver:self forKeyPath:@"estimatedProgress"];
    [_currentWebView removeObserver:self forKeyPath:@"URL"];
}

@end
