//
//  DropDownMenuView.m
//  简单app项目
//
//  Created by Guo on 2020/04/12.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "DropDownMenuView.h"
#import "WebViewController.h"

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@interface DropDownMenuView()<FFDropDownMenuViewDelegate>
@property (nonatomic, strong) NSString*  passWord;
@property (nonatomic, assign) NSInteger dropdownMenuTag;

@end

@implementation DropDownMenuView

/** 初始化下拉菜单 */
- (void)setupDropDownMenu {
    NSArray *modelsArray = [self getMenuModelsArray];
    
    self.dropdownMenu = [FFDropDownMenuView ff_DefaultStyleDropDownMenuWithMenuModelsArray:modelsArray menuWidth:FFDefaultFloat eachItemHeight:FFDefaultFloat menuRightMargin:FFDefaultFloat triangleRightMargin:FFDefaultFloat];
    
    //如果有需要，可以设置代理（非必须）
    self.dropdownMenu.delegate = self;

    self.dropdownMenu.ifShouldScroll = NO;

    [self.dropdownMenu setup];
    [self.dropdownMenu showMenu];
}

/** 获取菜单模型数组 */
- (NSArray *)getMenuModelsArray {
    //__weak typeof(self) weakSelf = self;
    
    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"创建新网页" menuItemIconName:@"" menuBlock:^(NSInteger rowNum){
        self.dropdownMenuTag = rowNum;
        self.passRowNumBlock(self.dropdownMenuTag);
    }];
    
    
    FFDropDownMenuModel *menuModel1 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"刷新" menuItemIconName:@""  menuBlock:^(NSInteger rowNum){
        self.dropdownMenuTag = rowNum;
        self.passRowNumBlock(self.dropdownMenuTag);
    }];
    
    FFDropDownMenuModel *menuModel2 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"关闭当前网页" menuItemIconName:@"" menuBlock:^(NSInteger rowNum){
        self.dropdownMenuTag = rowNum;
        self.passRowNumBlock(self.dropdownMenuTag);
    }];
    
    FFDropDownMenuModel *menuModel3 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"二维码扫描" menuItemIconName:@"" menuBlock:^(NSInteger rowNum){
        self.dropdownMenuTag = rowNum;
        self.passRowNumBlock(self.dropdownMenuTag);
    }];
    
    FFDropDownMenuModel *menuModel4 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"导航主页" menuItemIconName:@"" menuBlock:^(NSInteger rowNum){
        self.dropdownMenuTag = rowNum;
        self.passRowNumBlock(self.dropdownMenuTag);
    }];
    self.dropdownMenuTag = -1;
    
    FFDropDownMenuModel *menuModel5 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"購買記録" menuItemIconName:@"" menuBlock:^(NSInteger rowNum){
        self.dropdownMenuTag = rowNum;
        self.passRowNumBlock(self.dropdownMenuTag);
    }];
    self.dropdownMenuTag = -1;
    //UISwitch *
    FFDropDownMenuModel *menuModel6 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"系统状态" menuItemIconName:@"" menuBlock:^(NSInteger rowNum){
           self.dropdownMenuTag = rowNum;
           self.passRowNumBlock(self.dropdownMenuTag);
       }];
       self.dropdownMenuTag = -1;
    
    NSArray *menuModelArr = @[menuModel0, menuModel1, menuModel2, menuModel3, menuModel4, menuModel5, menuModel6];
    return menuModelArr;
}



/*


 */
@end
