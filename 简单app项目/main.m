//
//  main.m
//  简单app项目
//
//  Created by Guo on 2020/03/28.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
