//
//  DeveloperTaskView.h
//  简单app项目
//
//  Created by Guo on 2020/08/18.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define  DEVELOPER_TAKVIEW_TAG  9902
#define  TASK_VIEW_W  200
#define  TASK_VIEW_H  65

@interface DeveloperTaskView : UIView
{
    UILabel        *appCPU;
//    LevelMeter     *levelMeterViewapp;
//    LevelMeter     *levelMeterViewapp;
    UILabel        *threadCount;
    BOOL           smallBig;
    NSThread       *th;
    NSTimer        *updateTimer;
    NSDate         *startDate;
}

@property (nonatomic) NSTimeInterval curTimerInterval;

- (void)startThread: (NSTimeInterval)val;
- (void)endThread;
- (void)taskTimerSet: (NSTimeInterval)val;

@end


NS_ASSUME_NONNULL_END
