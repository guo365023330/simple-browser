//
//  SignUpViewController.m
//  简单app项目
//
//  Created by Guo on 2020/04/26.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "SignUpViewController.h"
#import <AVOSCloud.h>

@interface SignUpViewController ()
@property (nonatomic, strong) UILabel *userLb;
@property (nonatomic, strong) UILabel *pwLb1;
@property (nonatomic, strong) UILabel *pwLb2;
@property (nonatomic, strong) UILabel *teleLb;
@property (nonatomic, strong) UITextField *userTf;
@property (nonatomic, strong) UITextField *pwTf1;
@property (nonatomic, strong) UITextField *pwTf2;
@property (nonatomic, strong) UITextField *teleTf;
@property (nonatomic, strong) UIButton *confirmBtn;
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"注册界面";
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view, typically from a nib.
    //username
    CGRect rect_u = CGRectMake(60, 120, 120, 40);
    self.userLb = [[UILabel alloc] initWithFrame:(rect_u)];
    self.userLb.text = @"输入用户名";
    self.userLb.font = [UIFont systemFontOfSize:15];
    
    //password
    self.pwLb1 = [[UILabel alloc] init];
    self.pwLb1.frame = CGRectMake(60, 180, 120, 40);
    self.pwLb1.text = @"输入密码";
    self.pwLb1.font = [UIFont systemFontOfSize:15];
    
    //password 确认用
    self.pwLb2 = [[UILabel alloc] init];
    self.pwLb2.frame = CGRectMake(60, 240, 120, 40);
    self.pwLb2.text = @"再次输入密码";
    self.pwLb2.font = [UIFont systemFontOfSize:15];
    
    //username textfield
    self.userTf = [[UITextField alloc] init];
    self.userTf.frame = CGRectMake(160, 120, 140, 40);
    self.userTf.font = [UIFont systemFontOfSize:15];
    self.userTf.textAlignment = NSTextAlignmentLeft;
    self.userTf.borderStyle = UITextBorderStyleLine;
    
    //pw textfield
    self.pwTf1 = [[UITextField alloc] init];
    self.pwTf1.frame = CGRectMake(160, 180, 140, 40);
    self.pwTf1.font = [UIFont systemFontOfSize:15];
    self.pwTf1.textAlignment = NSTextAlignmentLeft;
    self.pwTf1.borderStyle = UITextBorderStyleLine;
    self.pwTf1.secureTextEntry = YES;
    
    //pw 确认用
    self.pwTf2 = [[UITextField alloc] init];
    self.pwTf2.frame = CGRectMake(160, 240, 140, 40);
    self.pwTf2.font = [UIFont systemFontOfSize:20];
    self.pwTf2.textAlignment = NSTextAlignmentLeft;
    self.pwTf2.borderStyle = UITextBorderStyleLine;
    self.pwTf2.secureTextEntry = YES;
    
    //确认注册button
    self.confirmBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.confirmBtn.frame = CGRectMake(140, 500, 100, 30);
    self.confirmBtn.backgroundColor = [UIColor yellowColor];
    //loginbn.bounds.size.height += 10;
    [self.confirmBtn setTitle:@"确认注册" forState:UIControlStateNormal];
    self.confirmBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.confirmBtn addTarget:self action:@selector(confirmClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.userLb];
    [self.view addSubview:self.pwLb1];
    [self.view addSubview:self.pwLb2];
    [self.view addSubview:self.userTf];
    [self.view addSubview:self.pwTf1];
    [self.view addSubview:self.pwTf2];
    [self.view addSubview:self.confirmBtn];
   // [self.view addSubview:self.regisBtn];
}

- (void) confirmClick{
    
    // 创建实例
    AVUser *user = [AVUser user];

    // 等同于 [user setObject:@"Tom" forKey:@"username"]
    user.username = self.userTf.text;
    user.password = self.pwTf1.text;

    // 可选
    user.email = @"365023330@qq.com";
    user.mobilePhoneNumber = @"+8618604047437";
    
    [user setObject:@"200" forKey:@"PointsData"];

    // 设置其他属性的方法跟 AVObject 一样
    /*
    [_userdata setObject:@"Li" forKey:@"username"];
    [_userdata setObject:@"123456" forKey:@"password"];
    */

    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            // 注册成功
            NSLog(@"注册成功。objectId：%@", user.objectId);
        } else {
            NSLog(@"注册失败 %@", error);
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
