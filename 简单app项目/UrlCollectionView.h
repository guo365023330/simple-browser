//
//  UrlCollectionView.h
//  简单app项目
//
//  Created by Guo on 2020/05/15.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UrlCollectionView : UIView<UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

//+ (instancetype) cellWithTableView:(UITableView*)tableView;
//@property (nonatomic, weak) id<>
- (void) createContentViewWithFrame:(CGRect)frame UrlArray:(NSMutableArray*)urlArray;
//- (void) createCollectionViewWIthFrame;
typedef void (^UrlCollectionCellBlock)(NSURL *url);
@property (nonatomic, copy) UrlCollectionCellBlock urlCollectionCellBlock;

@end

NS_ASSUME_NONNULL_END
