//
//  SceneDelegate.h
//  29 导航控制器基础
//
//  Created by Guo on 2020/03/12.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

