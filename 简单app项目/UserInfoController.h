//
//  UserInfoController.h
//  简单app项目
//
//  Created by Guo on 2020/06/21.
//  Copyright © 2020 Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "sqlite3.h"
#import <sqlite3.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoController : UIViewController

@property (nonatomic, assign) sqlite3 *UserDatabase;

-(void) openDb;
-(void) createTable;
-(void) insertData;
@end

NS_ASSUME_NONNULL_END
