//
//  DropDownMenuView.h
//  简单app项目
//
//  Created by Guo on 2020/04/12.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "FFDropDownMenuView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DropDownMenuView : FFDropDownMenuView

/** 下拉菜单 */
@property (nonatomic, strong) FFDropDownMenuView *dropdownMenu;
@property (copy) void (^passRowNumBlock) (NSInteger);//nonatomic代表getter setter加锁 多线程同时访问容易出问题

- (void) setupDropDownMenu;
@end

NS_ASSUME_NONNULL_END
