//
//  UserInfoController.m
//  简单app项目
//
//  Created by Guo on 2020/06/21.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "UserInfoController.h"

@interface UserInfoController ()

@end

@implementation UserInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self openDb];
    [self createTable];
    [self insertData];
}

-(void)openDb{
    NSArray *myPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [myPaths objectAtIndex:0];
    NSString *dbPath = [docPath stringByAppendingString:@"/UserData.db"];
    sqlite3_open([dbPath UTF8String], &_UserDatabase);
    NSLog(@"opened database");
}

-(void) createTable {
    const char *createSql = "create table if not exists UserData(ID INTEGER PRIMARY KEY AUTOINCREMENT, Name text, Tel double, Payment int, BuyDate date, Discount bool, Points int)";
    /*
     第三个参数是执行语句后的回调函数
     第四个是传递给回调函数使用的参数
     第五个用语存储执行时产生的错误信息
    */
    char *errorMsg = NULL;
    int rc = sqlite3_exec(_UserDatabase, createSql, NULL, NULL, &errorMsg);
    if (rc != SQLITE_OK){
        NSLog(@"fail create data");
    }

   
}

-(void) insertData{
    const char *insertSql = "INSERT INTO UserData(ID, Name, Tel) VALUES(1, 'GUO Yuan', 13998113743)";
    sqlite3_exec(_UserDatabase, insertSql, NULL, NULL, nil);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
