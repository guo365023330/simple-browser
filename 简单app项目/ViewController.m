//
//  ViewController.m
//  简单app项目
//
//  Created by Guo on 2020/03/28.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "ViewController.h"
#import "WebViewController.h"
#import "SignUpViewController.h"
#import <AVOSCloud.h>//引入系统库文件 只查找库目录 尤其是cocoapod导入的
#import <UserNotifications/UserNotifications.h>

@interface ViewController ()<UNUserNotificationCenterDelegate>
@property (nonatomic, strong) NSString*  userName;
@property (nonatomic, strong) NSString*  passWord;
@property (nonatomic, strong) AVObject*  userdata;
@property (nonatomic, strong) UILabel *userLb;
@property (nonatomic, strong) UILabel *pwLb;
@property (nonatomic, strong) UITextField *userTf;
@property (nonatomic, strong) UITextField *pwTf;
@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) UIButton *regisBtn;
@property (nonatomic, strong) UIButton *notifiBtn;
//@property (nonatomic, strong) NSNumber *badge;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.title = @"登录界面";
    // Do any additional setup after loading the view, typically from a nib.
    //username
    CGRect rect_u = CGRectMake(70, 120, 80, 40);
    self.userLb = [[UILabel alloc] initWithFrame:(rect_u)];
    self.userLb.text = @"用户名";
    self.userLb.font = [UIFont systemFontOfSize:20];
    
    //password
    self.pwLb = [[UILabel alloc] init];
    self.pwLb.frame = CGRectMake(70, 180, 80, 40);
    self.pwLb.text = @"密码";
    self.pwLb.font = [UIFont systemFontOfSize:20];
    
    //username textfield
    self.userTf = [[UITextField alloc] init];
    self.userTf.frame = CGRectMake(190, 120, 140, 40);
    self.userTf.font = [UIFont systemFontOfSize:20];
    self.userTf.textAlignment = NSTextAlignmentLeft;
    self.userTf.borderStyle = UITextBorderStyleLine;
    self.userTf.text = @"guo";
    
    //pw textfield
    self.pwTf = [[UITextField alloc] init];
    self.pwTf.frame = CGRectMake(190, 180, 140, 40);
    self.pwTf.font = [UIFont systemFontOfSize:20];
    self.pwTf.textAlignment = NSTextAlignmentLeft;
    self.pwTf.borderStyle = UITextBorderStyleLine;
    self.pwTf.secureTextEntry = YES;
    self.pwTf.text = @"494919";
    
    //登陆和注册button
    self.loginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.loginBtn.frame = CGRectMake(90, 240, 60, 30);
    self.loginBtn.backgroundColor = [UIColor yellowColor];
    //loginbn.bounds.size.height += 10;
    [self.loginBtn setTitle:@"登陆" forState:UIControlStateNormal];
    self.loginBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.loginBtn addTarget:self action:@selector(pwCheck) forControlEvents:UIControlEventTouchUpInside];
    
    self.regisBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.regisBtn.frame = CGRectMake(220, 240, 60, 30);
    self.regisBtn.backgroundColor = [UIColor grayColor];
    [self.regisBtn setTitle:@"注册" forState:UIControlStateNormal];
    self.regisBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.regisBtn addTarget:self action:@selector(regisClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.notifiBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    self.notifiBtn.frame = CGRectMake(90, 290, 60, 30);
    self.notifiBtn.backgroundColor = [UIColor redColor];
    [self.notifiBtn setTitle:@"本地通知" forState:UIControlStateNormal];
    self.notifiBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.notifiBtn addTarget:self action:@selector(createNotifi) forControlEvents:UIControlEventTouchUpInside];
   
    
    //登陆和注册button
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.frame = CGRectMake(90, 440, 60, 40);
    btn.backgroundColor = [UIColor yellowColor];
    //loginbn.bounds.size.height += 10;
    [btn setTitle:@"测试" forState:UIControlStateNormal];
    
    [self.view addSubview:self.userLb];
    [self.view addSubview:self.pwLb];
    [self.view addSubview:self.userTf];
    [self.view addSubview:self.pwTf];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.regisBtn];
    [self.view addSubview:self.notifiBtn];
}

//本地消息推送
- (void) createNotifi{
    if (@available(iOS 10.0, *)) {//iOS 10 以后
        [[UNUserNotificationCenter currentNotificationCenter]getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            UNNotificationSetting notificationSetting = settings.notificationCenterSetting;
            if (notificationSetting == UNNotificationSettingEnabled) {//用户打开通知允许
                NSString  *requestID = @"testId";
                //创建UNUserNotificationCenter 设置推送模式和代理！
                UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                center.delegate = self;
                //iOS 10 使用以下方法注册，才能得到授权
                [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound +UNAuthorizationOptionBadge)
                                      completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                          if (!error) {
                                              NSLog(@"succeeded!");
                                          }
                                      }];
                
                //设置推送内容
                UNMutableNotificationContent *content = [UNMutableNotificationContent new];
                content.title = @"推送中心标题";
                content.subtitle = @"副标题";
                content.body  = @"这是UNUserNotificationCenter信息中心";
                content.badge = @1;
                content.categoryIdentifier = @"categoryIdentifier";
                content.sound = [UNNotificationSound defaultSound];
                
                //        需要解锁显示，红色文字。点击不会进app。
                //        UNNotificationActionOptionAuthenticationRequired = (1 << 0),
                //
                //        黑色文字。点击不会进app。
                //        UNNotificationActionOptionDestructive = (1 << 1),
                //
                //        黑色文字。点击会进app。
                //        UNNotificationActionOptionForeground = (1 << 2),
                     
                UNNotificationAction *action = [UNNotificationAction actionWithIdentifier:@"enterApp"
                                                                                    title:@"进入应用"
                                                                                  options:UNNotificationActionOptionForeground];
                UNNotificationAction *clearAction = [UNNotificationAction actionWithIdentifier:@"destructive"
                                                                                         title:@"忽略2"
                                                                                       options:UNNotificationActionOptionDestructive];
                UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"categoryIdentifier" actions:@[action,clearAction] intentIdentifiers:@[requestID] options:UNNotificationCategoryOptionNone];
                [center setNotificationCategories:[NSSet setWithObject:category]];

                //设置推送方式
                UNTimeIntervalNotificationTrigger *timeTrigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:7 repeats:NO];
                
                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:requestID content: content trigger:timeTrigger];
                
                //添加推送request
                [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) { }];
                
                //[center removePendingNotificationRequestsWithIdentifiers:@[requestID]];
                //获取当前的通知设置，UNNotificationSettings 是只读对象，不能直接修改，只能通过以下方法获取
                //[center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            }
        }];
    }
}

//应用内展示通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    //功能：可设置是否在应用内弹出通知
    completionHandler(UNNotificationPresentationOptionBadge + UNNotificationPresentationOptionSound);
}

 //程序外显示通知时
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    NSLog(@"============didReceiveNotificationResponse");
    NSString *categoryID = response.notification.request.content.categoryIdentifier;
    if ([categoryID isEqualToString:@"categoryIdentifier"]) {
        if ([response.actionIdentifier isEqualToString:@"enterApp"]) {
            if (@available(iOS 10.0, *)) {
                
             } else {
                 // Fallback on earlier versions
             }
        }else{
             NSLog(@"No======");
        }
    }
    completionHandler();
    [center removeAllDeliveredNotifications];
}


//(NSString *) userName, passWord
- (void) pwCheck{
     AVUser *user = [AVUser user];
    user.username = self.userTf.text;
    user.password = self.pwTf.text;
    //_userdata = [AVObject objectWithClassName:@"_User"];
    [AVUser logInWithUsernameInBackground:self.userTf.text password:self.pwTf.text block:^(AVUser *user, NSError *error) {
        if (user != nil) {
            NSLog(@"登录成功");
            WebViewController *webView = [[WebViewController alloc] init];
            [self.navigationController pushViewController:webView animated:YES];
        } else {
            NSLog(@"账号或密码错误");
        }
    }];
    /* plist数据取得并验证
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"userData" ofType:@"plist"];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    NSString *tmpUserName = [dic objectForKey: @"userName"];
    NSString *tmpPW = [dic objectForKey: @"userPw"];
     
    if ([_userName isEqualToString: self.userdata[@"username"]]){
        if ([_passWord isEqualToString: self.userdata[@"password"]]){
            NSLog(@"登录成功");
            WebViewController *webView = [[WebViewController alloc] init];
            // [self presentViewController:webView animated:YES completion:nil]; 这种方法不加在navigator
            [self.navigationController pushViewController:webView animated:YES];
        }else{
            NSLog(@"账号或密码错误");
        }
    }else{
        NSLog(@"账号或密码错误");
    }
     */
}

- (void) regisClick{
    
    SignUpViewController *registVC = [[SignUpViewController alloc] init];
    [self.navigationController pushViewController:registVC animated:YES];
}

@end
