//
//  DeveloperTaskView.m
//  简单app项目
//
//  Created by Guo on 2020/08/18.
//  Copyright © 2020 Guo. All rights reserved.
//

#import "DeveloperTaskView.h"
#import <mach/mach.h>

@implementation DeveloperTaskView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initWithFrame:(CGRect)frame
{
    CGRect frm;
    self = [super initWithFrame:frame];
    if (self) {
        self.tag = DEVELOPER_TAKVIEW_TAG;
        smallBig = NO;
        self.autoresizesSubviews = YES;
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.7];
        frm = CGRectMake(23, 5, 150, 10);
        appCPU = [[UILabel alloc] initWithFrame:frm];
        appCPU.backgroundColor = [UIColor clearColor];
        appCPU.textColor = [UIColor cyanColor];
        appCPU.text = @"计算中";
        
        frm.origin.y += 15;
        self.autoresizesSubviews = YES;
        threadCount = [[UILabel alloc] initWithFrame:frm];
        threadCount.backgroundColor = [UIColor clearColor];
        threadCount.textColor = [UIColor cyanColor];
        threadCount.text = @"统计中";
        [self addSubview:appCPU];
        [self addSubview:threadCount];
    }
    return self;
}

- (void)startThread: (NSTimeInterval)val
{
    if (th!=nil)
        return;
    startDate = [NSDate date];
    th = [[NSThread alloc] initWithTarget:self selector:@selector(ThreadFunc:) object:[NSNumber numberWithFloat:val]];
    if (th!=nil){
        th.name = @"DeveloperTaskView";
        [th start];
    }
}

- (void)endThread
{
    if (th == nil)
        return;
    if (updateTimer != nil)
        [updateTimer invalidate];
    updateTimer = nil;
    [th cancel];
    th = nil;
}

- (void)ThreadFunc: (id)object
{
    @autoreleasepool {
        updateTimer = [NSTimer timerWithTimeInterval:[object floatValue] target:self selector:@selector(doTimer) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop]addTimer:updateTimer forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
    }
}

- (void)doTimer
{
    if (startDate == nil)
        return;
    float fVal;
    NSTimeInterval tmp = [[NSDate date]timeIntervalSinceDate:startDate];
    if (tmp < 0.5) //第一次表示等0.5秒
        return;
    fVal = [self getAppCpuUSage]*100;
    appCPU.text = [NSString stringWithFormat:@"当前使用率：%f \n", fVal];
    if (threadCount != nil) {
        mach_msg_type_number_t val = [self getThreadCount];
        threadCount.text = [NSString stringWithFormat:@"ストレット数：%u", val];
    }
}

- (float)getAppCpuUSage{
    float tot_cpu = 0;
    mach_msg_type_number_t thread_count;
    thread_array_t thread_list;

    //获取当前执行的线程数组和个数
    kern_return_t kr = task_threads(mach_task_self(), &thread_list, &thread_count);
    if (kr!= KERN_SUCCESS) {
        return 0.0f;
    }
    thread_info_data_t thinfo;
    mach_msg_type_number_t  thread_info_count;
    thread_basic_info_t basic_info_th;
    //遍历所有线程
    for (int j = 0 ; j < thread_count; j++) {
        //数量
        thread_info_count = THREAD_INFO_MAX;
        //获取线程的基础信息和信息个数
        kr = thread_info(thread_list[j], THREAD_BASIC_INFO, (thread_info_t)thinfo, &thread_info_count);
        if (kr!= KERN_SUCCESS) {
            return 0.0f;
        }
        //转换基础信息类型
        basic_info_th = (thread_basic_info_t)thinfo;
        //判断是否闲置线程
        if (!(basic_info_th->flags & TH_FLAGS_IDLE)){
            tot_cpu += basic_info_th->cpu_usage/(float)TH_USAGE_SCALE*100.0;
        }
    }
    return tot_cpu/100.f;
}

- (mach_msg_type_number_t)getThreadCount
{
    kern_return_t          rtn;
    mach_msg_type_number_t count = 0;
    thread_act_array_t     threads;
    int                    i;
    
    task_threads(mach_task_self(), &threads, &count);
    for(i = 0; i < count; i++){
        if(rtn = mach_port_deallocate(mach_task_self(), threads[i]))
            break;
    }
    vm_deallocate(mach_task_self(), (vm_offset_t)threads, count*sizeof(thread_t));
    return (count);
}
@end
